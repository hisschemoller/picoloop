import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './Nav.css';

import { NAV_SEQUENCER, NAV_STUDIO } from '../../../constants/controlIds';
import Control from '../Control';

import * as navActions from '../../../actions/nav.actions';

const mapStateToProps = ({ nav }) => ({
  section: nav.section,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...navActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(mapStateToProps, mapDispatchToProps)
class Nav extends PureComponent {
  static propTypes = {
    section: PropTypes.string,
  };

  static defaultProps = {
    section: null,
  };

  render() {
    const { section } = this.props;

    return (
      <div className={s.root}>
        <Control
          id={NAV_SEQUENCER}
          isSlider={false}
          selected={section === NAV_SEQUENCER}
          text="seq"
        />
        <Control
          id={NAV_STUDIO}
          isSlider={false}
          selected={section === NAV_STUDIO}
          text="inst"
        />
      </div>
    );
  }
}

export default Nav;
