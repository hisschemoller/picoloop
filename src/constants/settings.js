/* eslint-disable import/prefer-default-export */

export const DEFAULT_BPM = 115;
export const DEFAULT_TRACK_LENGTH = 16;
export const DEFAULT_GAIN = 1;
export const DEFAULT_PITCH = 60;
export const DEFAULT_TRANSPOSE = 0;
export const LOOKAHEAD = 500;
export const PPQN = 16;
export const SLIDER_TRACK_LENGTH = 127;
export const TRACKS = 1;
export const TRANSPOSE_MAX = 12;
