import { INITIALIZE, MODAL_HIDE, MODAL_SHOW } from '../constants';

const initialState = {
  initialized: false,
  modalType: null,
};

export default function picoloop(state = initialState, action) {
  switch (action.type) {
    case INITIALIZE:
      return {
        ...state,
        initialized: true,
      };
    case MODAL_HIDE:
      return {
        ...state,
        modalType: initialState.modalType,
      };
    case MODAL_SHOW:
      return {
        ...state,
        modalType: action.modalType,
      };
    default:
      return state;
  }
}
