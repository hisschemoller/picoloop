import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './StepEditor.css';

import { TRACK_GAIN, TRACK_TRANSPOSE } from '../../../constants/controlIds';
import Control from '../Control';

import * as stepeditorActions from '../../../actions/stepeditor.actions';

const mapStateToProps = ({ stepeditor }) => ({
  editorType: stepeditor.editorType,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...stepeditorActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(mapStateToProps, mapDispatchToProps)
class StepEditor extends PureComponent {
  static propTypes = {
    editorType: PropTypes.string,
  };

  static defaultProps = {
    editorType: null,
  };

  render() {
    const { editorType } = this.props;

    return (
      <div className={s.root}>
        <Control
          id={TRACK_GAIN}
          isSlider
          selected={editorType === TRACK_GAIN}
          text="v"
        />
        <Control
          id={TRACK_TRANSPOSE}
          isSlider
          selected={editorType === TRACK_TRANSPOSE}
          text="t"
        />
      </div>
    );
  }
}

export default StepEditor;
