import React from 'react';
import Home from '../../components/templates/Home';

async function action() {
  return {
    title: 'React Starter Kit',
    chunks: ['home'],
    component: <Home />,
  };
}

export default action;
