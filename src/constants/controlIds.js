/* eslint-disable import/prefer-default-export */

export const BPM = 'BPM';
export const NAV_SEQUENCER = 'NAV_SEQUENCER';
export const NAV_STUDIO = 'NAV_STUDIO';
export const PLAY = 'PLAY';
export const TRACK_GAIN = 'TRACK_GAIN';
export const TRACK_TRANSPOSE = 'TRACK_TRANSPOSE';
