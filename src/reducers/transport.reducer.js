import { TRANSPORT_BPM, TRANSPORT_STEP, TRANSPORT_TOGGLE } from '../constants';
import { DEFAULT_BPM } from '../constants/settings';

const initialState = {
  bpm: DEFAULT_BPM,
  isRunning: false,
  nowToStep: 0,
  stepIndex: 0,
};

export default function score(state = initialState, action) {
  switch (action.type) {
    case TRANSPORT_BPM:
      return {
        ...state,
        bpm: state.bpm + action.normalValueChange * 10,
      };
    case TRANSPORT_STEP:
      return {
        ...state,
        nowToStep: action.nowToStep,
        stepIndex: action.stepIndex,
      };
    case TRANSPORT_TOGGLE:
      return {
        ...state,
        isRunning: !state.isRunning,
        stepIndex: !state.isRunning ? null : 0,
      };
    default:
      return state;
  }
}
