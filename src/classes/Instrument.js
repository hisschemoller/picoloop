import { TRANSPOSE_MAX } from '../constants/settings';

/**
 * Web Audio instrument base class.
 *
 * @export
 * @class Instrument
 */
export default class Instrument {
  /**
   * Convert MIDI pitch to frequency.
   *
   * @static
   * @param {Number} pitch Note number where 60 is middle C.
   * @returns {Number} Frequency in Hertz.
   * @memberof Instrument
   */
  static mtof(pitch) {
    if (pitch <= -1500) return 0;
    else if (pitch > 1499) return 3.282417553401589e38;
    return 440.0 * 2 ** ((Math.floor(pitch) - 69) / 12.0);
  }

  constructor(ctx) {
    this.ctx = ctx;
    this.setup();
  }

  setup() {
    this.gain = this.ctx.createGain();
    this.gain.connect(this.ctx.destination);
  }

  destroy() {
    this.gain.disconnect(this.ctx.destination);
    this.gain = null;
    this.ctx = null;
  }

  playNote(step, nowToStep) {
    if (step.selected) {
      const when = this.ctx.currentTime + nowToStep / 1000;
      this.createVoice(step, when);
    }
  }

  /**
   * Single voice plays a sound and removes itself
   * after the sound has finished.
   *
   * @param {Object} step Sound parameter object.
   * @param {Number} when Offset in seconds to playback start.
   * @memberof Instrument
   */
  createVoice(step, when) {
    const gain = this.ctx.createGain();
    gain.gain.setValueAtTime(step.gain, when);
    gain.gain.exponentialRampToValueAtTime(0.001, when + 0.8);
    gain.connect(this.gain);

    const osc = this.ctx.createOscillator();
    osc.frequency.setValueAtTime(
      Instrument.mtof(step.pitch + Math.round(step.transpose * TRANSPOSE_MAX)),
      when,
    );
    osc.onended = () => {
      gain.disconnect(this.gain);
    };
    osc.connect(gain);
    osc.start(when);
    osc.stop(when + 0.8);
  }
}
