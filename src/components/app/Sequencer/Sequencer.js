import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './Sequencer.css';

import Control from '../Control';

const mapStateToProps = ({ score, transport }) => ({
  isRunning: transport.isRunning,
  nowToStep: transport.nowToStep,
  stepIndex: transport.stepIndex,
  steps: score.steps,
  trackId: score.trackId,
  tracks: score.tracks,
});

@withStyles(normalizeCss, s)
@connect(mapStateToProps)
class Sequencer extends Component {
  static propTypes = {
    isRunning: PropTypes.bool,
    stepIndex: PropTypes.number,
    steps: PropTypes.shape(),
    trackId: PropTypes.string,
    tracks: PropTypes.shape(),
  };

  static defaultProps = {
    isRunning: false,
    stepIndex: 0,
    steps: null,
    trackId: null,
    tracks: {},
  };

  constructor(props) {
    super(props);

    this.mounted = false;

    this.state = {
      timedStepIndex: 0,
    };
  }

  componentDidMount() {
    this.mounted = true;
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { nowToStep, stepIndex, steps, trackId } = nextProps;
    if (steps !== this.props.steps || trackId !== this.props.trackId) {
      return true;
    }
    if (stepIndex !== this.props.stepIndex) {
      setTimeout(() => {
        if (this.mounted) {
          this.setState({
            timedStepIndex: stepIndex,
          });
        }
      }, nowToStep);
      return false;
    }
    if (nextState.timedStepIndex !== this.state.timedStepIndex) {
      return true;
    }
    return false;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    const { isRunning, steps, trackId, tracks } = this.props;
    const { timedStepIndex } = this.state;
    const track = tracks.byId[trackId];

    return (
      <div className={s.root}>
        {track &&
          track.stepIds &&
          track.stepIds.map((stepId, index, stepIds) => (
            <Control
              active={isRunning && timedStepIndex % stepIds.length === index}
              id={index.toString()}
              isSlider
              key={stepId}
              selected={steps.byId[stepId].selected}
            />
          ))}
      </div>
    );
  }
}

export default Sequencer;
