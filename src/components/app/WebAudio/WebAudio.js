import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import * as picoloopActions from '../../../actions/picoloop.actions';
import { MODAL_UNLOCK_IOS_AUDIO } from '../../../constants/modaltypes';
import Instrument from '../../../classes/Instrument';

const mapStateToProps = ({ score, picoloop, transport }) => ({
  initialized: picoloop.initialized,
  nowToStep: transport.nowToStep,
  patternId: score.patternId,
  patterns: score.patterns,
  stepIndex: transport.stepIndex,
  steps: score.steps,
  trackId: score.trackId,
  tracks: score.tracks,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...picoloopActions }, dispatch);

let audioContext;

@connect(mapStateToProps, mapDispatchToProps)
class WebAudio extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired,
    initialized: PropTypes.bool,
    nowToStep: PropTypes.number,
    showModal: PropTypes.func.isRequired,
    stepIndex: PropTypes.number,
    steps: PropTypes.shape(),
    trackId: PropTypes.string,
    tracks: PropTypes.shape(),
  };

  static defaultProps = {
    initialized: false,
    nowToStep: 0,
    stepIndex: null,
    steps: null,
    trackId: null,
    tracks: null,
  };

  /**
   * Get the step to play for a given track.
   *
   * @static
   * @param {Object} tracks All tracks of all patterns.
   * @param {String} trackId Track ID.
   * @param {Object} steps All steps of all tracks of all patterns.
   * @param {Number} stepIndex 'Absolute' step index since playback started.
   * @returns {Object} Step object for the instrument to play.
   * @memberof Studio
   */
  static getStep(tracks, trackId, steps, stepIndex) {
    const { stepIds } = tracks.byId[trackId];
    return steps.byId[stepIds[stepIndex % stepIds.length]];
  }

  constructor(props) {
    super(props);
    this.instruments = [];
    this.stepIndex = null;
  }

  /**
   * When the component mounts: Create an AudioContext.
   */
  componentDidMount() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    this.detectIOSDevice();
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.stepIndex !== nextProps.stepIndex) {
      return true;
    } else if (
      this.props.initialized === false &&
      nextProps.initialized === true
    ) {
      return true;
    }
    return false;
  }

  componentWillUnmount() {
    audioContext.close();
  }

  detectIOSDevice() {
    const { initialize, showModal } = this.props;
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
      showModal(MODAL_UNLOCK_IOS_AUDIO);
    } else {
      initialize();
    }
  }

  render() {
    const {
      initialized,
      nowToStep,
      stepIndex,
      steps,
      trackId,
      tracks,
    } = this.props;

    // initialize the instruments
    if (initialized && !this.instruments.length) {
      for (let i = 0, n = tracks.allIds.length; i < n; i += 1) {
        this.instruments.push(new Instrument(audioContext));
      }
    }

    // new step to play
    if (stepIndex !== this.stepIndex) {
      this.stepIndex = stepIndex;
      for (let i = 0, n = tracks.allIds.length; i < n; i += 1) {
        const step = WebAudio.getStep(tracks, trackId, steps, stepIndex);
        this.instruments[i].playNote(step, nowToStep);
      }
    }

    return null;
  }
}

export function getAudioContext() {
  return audioContext;
}

export default WebAudio;
