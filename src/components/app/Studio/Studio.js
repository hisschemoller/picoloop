import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Instrument from '../Instrument';

const mapStateToProps = ({ score, picoloop, transport }) => ({
  initialized: picoloop.initialized,
  nowToStep: transport.nowToStep,
  patternId: score.patternId,
  patterns: score.patterns,
  stepIndex: transport.stepIndex,
  steps: score.steps,
  trackId: score.trackId,
  tracks: score.tracks,
});

@connect(mapStateToProps)
class Studio extends Component {
  static propTypes = {
    initialized: PropTypes.bool,
    stepIndex: PropTypes.number,
  };

  static defaultProps = {
    initialized: false,
    stepIndex: null,
  };

  /**
   * Get the step to play for a given track.
   *
   * @static
   * @param {Object} tracks All tracks of all patterns.
   * @param {String} trackId Track ID.
   * @param {Object} steps All steps of all tracks of all patterns.
   * @param {Number} stepIndex 'Absolute' step index since playback started.
   * @returns {Object} Step object for the instrument to play.
   * @memberof Studio
   */
  static getStep(tracks, trackId, steps, stepIndex) {
    const { stepIds } = tracks.byId[trackId];
    return steps.byId[stepIds[stepIndex % stepIds.length]];
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.stepIndex !== nextProps.stepIndex) {
      return true;
    } else if (
      this.props.initialized === false &&
      nextProps.initialized === true
    ) {
      return true;
    }
    return false;
  }

  render() {
    const { patternId, patterns } = {
      ...this.props,
    };

    return (
      <div>
        {patterns &&
          patternId &&
          patterns.byId[patternId].trackIds.map(trackId => (
            <Instrument key={trackId} />
          ))}
      </div>
    );
  }
}

export default Studio;
