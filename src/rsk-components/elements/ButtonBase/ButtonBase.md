Basic ButtonBase component

```js
<ButtonBase
  onClick={() => {
    alert('clicked button!');
  }}
>
  ButtonBase
</ButtonBase>
```
