import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import themableWithStyles from 'themableWithStyles';

import Link from '../../elements/Link';

import styles from './ButtonBase.css';

@themableWithStyles(styles)
class ButtonBase extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * Use that property to pass a ref callback to the native button component.
     */
    buttonRef: PropTypes.func,
    /**
     * The component used for the root node.
     * Either a string to use a DOM element or a component.
     * The default value is a `button`.
     */
    component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    /**
     * If `true`, the base button will be disabled.
     */
    disabled: PropTypes.bool,
    /**
     * @ignore
     */
    role: PropTypes.string,
    /**
     * @ignore
     */
    tabIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /**
     * @ignore
     */
    type: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    buttonRef: null,
    tabIndex: '0',
    disabled: false,
    role: 'button',
    component: null,
    type: 'button',
    css: {
      root: styles.root,
      disabled: styles.disabled,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      buttonRef,
      component,
      disabled,
      tabIndex,
      type,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      {
        [css.disabled]: disabled,
      },
      classNameProp,
    );

    const buttonProps = {};

    let ComponentProp = component;

    if (!ComponentProp) {
      if (rest.to) {
        ComponentProp = Link;
      } else if (rest.href) {
        ComponentProp = 'a';
      } else {
        ComponentProp = 'button';
      }
    }

    if (ComponentProp === 'button') {
      buttonProps.type = type || 'button';
      buttonProps.disabled = disabled;
    }

    return (
      <ComponentProp
        tabIndex={disabled ? '-1' : tabIndex}
        className={className}
        ref={buttonRef}
        {...buttonProps}
        {...rest}
      >
        {children}
      </ComponentProp>
    );
  }
}

export default ButtonBase;
