/* eslint-disable import/prefer-default-export */

import { SELECT_EDITOR_TYPE } from '../constants';

import makeActionCreator from './action-utils';

export const selectEditorType = makeActionCreator(
  SELECT_EDITOR_TYPE,
  'editorType',
);
