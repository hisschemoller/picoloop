import React, { PureComponent } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import normalizeCss from 'normalize.css';
import s from './PicoLoop.css';

import { NAV_SEQUENCER, NAV_STUDIO } from '../../../constants/controlIds';
import ModalContainer from '../../organisms/ModalContainer';
import Nav from '../Nav';
import Sequencer from '../Sequencer';
import Slider from '../Slider';
import StepEditor from '../StepEditor';
import Studio from '../Studio';
import Transport from '../Transport';
import WebAudio from '../WebAudio';

import * as controlActions from '../../../actions/control.actions';
import * as scoreActions from '../../../actions/score.actions';

const mapStateToProps = ({ control, nav }) => ({
  controlId: control.controlId,
  section: nav.section,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...controlActions, ...scoreActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(mapStateToProps, mapDispatchToProps)
class PicoLoop extends PureComponent {
  static propTypes = {
    controlEnd: PropTypes.func.isRequired,
    controlId: PropTypes.string,
    controlMove: PropTypes.func.isRequired,
    controlStart: PropTypes.func.isRequired,
    createPattern: PropTypes.func.isRequired,
    section: PropTypes.string.isRequired,
    selectPattern: PropTypes.func.isRequired,
    selectTrack: PropTypes.func.isRequired,
  };

  static defaultProps = {
    controlId: null,
  };

  componentDidMount() {
    const { createPattern, selectPattern, selectTrack } = this.props;
    createPattern();
    selectPattern(0);
    selectTrack(0);

    this.touchData = {
      loc: null,
      rect: null,
    };
    this.isTouchDevice = 'ontouchstart' in document.documentElement;
  }

  getTouchIsOnButton() {
    const { loc, rect } = this.touchData;
    return (
      loc &&
      rect &&
      loc.x > rect.left &&
      loc.x < rect.right &&
      loc.y > rect.top &&
      loc.y < rect.bottom
    );
  }

  render() {
    const { controlEnd, controlMove, controlStart, section } = this.props;

    return (
      <div
        className={s.root}
        onMouseDown={e => {
          const el = e.target.closest('[data-control-id]');
          if (this.isTouchDevice) {
            return;
          }
          if (el) {
            const { controlId, isSelected, isSlider } = el.dataset;
            const rect = el.getBoundingClientRect();
            controlStart(
              controlId,
              isSelected,
              isSlider,
              e.clientX,
              e.clientY,
              e.clientX - rect.left,
              e.clientY - rect.top,
            );
          }
        }}
        onMouseMove={e => {
          const { controlId } = this.props;
          if (controlId) {
            controlMove(e.clientX, e.clientY);
          }
        }}
        onMouseUp={e => {
          if (this.isTouchDevice) {
            return;
          }
          const isOnControl = e.target.closest('[data-control-id]');
          controlEnd(isOnControl);
        }}
        onTouchEnd={() => {
          const { controlId } = this.props;
          if (controlId) {
            const isOnControl = this.getTouchIsOnButton();
            controlEnd(isOnControl);
          }
        }}
        onTouchMove={e => {
          e.stopPropagation();
          const { controlId } = this.props;
          if (controlId) {
            this.touchData.loc = {
              x: e.touches[0].clientX,
              y: e.touches[0].clientY,
            };
            controlMove(e.touches[0].clientX, e.touches[0].clientY);
          }
        }}
        onTouchStart={e => {
          e.stopPropagation();
          const el = e.target.closest('[data-control-id]');
          if (el) {
            this.touchData.rect = el.getBoundingClientRect();
            this.touchData.loc = {
              x: e.touches[0].clientX,
              y: e.touches[0].clientY,
            };
            const { controlId, isSelected, isSlider } = el.dataset;
            const rect = el.getBoundingClientRect();
            controlStart(
              controlId,
              isSelected,
              isSlider,
              e.touches[0].clientX,
              e.touches[0].clientY,
              e.touches[0].clientX - rect.left,
              e.touches[0].clientY - rect.top,
            );
          }
        }}
        role="button"
        tabIndex={0}
      >
        <div className={s.grid}>
          <div className={s.nav}>
            <Nav />
          </div>
          <div className={s.patterns} />
          <div className={s.transport}>
            <Transport />
          </div>
          <div className={s.sequencer}>
            {section === NAV_SEQUENCER && <Sequencer />}
            {section === NAV_STUDIO && <Studio />}
          </div>
          <div className={s.right} />
          <div className={s.stepeditor}>
            {section === NAV_SEQUENCER && <StepEditor />}
          </div>
          <div className={s.tracks} />
        </div>
        <ModalContainer />
        <Slider />
        <WebAudio />
      </div>
    );
  }
}

export default PicoLoop;
