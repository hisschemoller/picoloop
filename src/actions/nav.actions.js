/* eslint-disable import/prefer-default-export */

import { NAV_SELECT } from '../constants';

import makeActionCreator from './action-utils';

export const navSelect = makeActionCreator(NAV_SELECT, 'section');
