import { NAV_SELECT } from '../constants';
import { NAV_SEQUENCER } from '../constants/controlIds';

const initialState = {
  section: NAV_SEQUENCER,
};

export default function stepeditor(state = initialState, action) {
  switch (action.type) {
    case NAV_SELECT:
      return {
        ...state,
        section: action.section,
      };
    default:
      return state;
  }
}
