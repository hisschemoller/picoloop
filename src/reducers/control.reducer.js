import { CONTROL_END, CONTROL_MOVE, CONTROL_START } from '../constants';
import { SLIDER_TRACK_LENGTH } from '../constants/settings';

const initialState = {
  controlId: null,
  controlIsSelected: false,
  controlIsSlider: false,
  controlOffsetX: 0,
  controlOffsetY: 0,
  controlStartX: 0,
  controlStartY: 0,
  controlX: 0,
  controlY: 0,
  isDown: false,
  isTap: false,
  relativeValue: 0,
};

export default function control(state = initialState, action) {
  switch (action.type) {
    case CONTROL_END:
      return {
        ...state,
        controlId: null,
        controlIsSelected: false,
        controlIsSlider: false,
        controlX: 0,
        controlY: 0,
        isDown: false,
      };
    case CONTROL_MOVE:
      return {
        ...state,
        controlX: action.x,
        controlY: action.y,
        isTap: false,
        relativeValue: Math.max(
          -1,
          Math.min(
            (state.controlStartY - state.controlY) / SLIDER_TRACK_LENGTH,
            1,
          ),
        ),
      };
    case CONTROL_START:
      return {
        ...state,
        controlId: action.id,
        controlIsSelected: action.isSelected,
        controlIsSlider: action.isSlider === 'true',
        controlX: action.x,
        controlY: action.y,
        controlStartX: action.x,
        controlStartY: action.y,
        controlOffsetX: action.offsetX,
        controlOffsetY: action.offsetY,
        isDown: true,
        isTap: true,
        relativeValue: 0,
      };
    default:
      return state;
  }
}
