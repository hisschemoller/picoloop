import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './ModalUnlockIOSAudio.css';

import Modal from '../../organisms/Modal';
import * as picoloopActions from '../../../actions/picoloop.actions';

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...picoloopActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(null, mapDispatchToProps)
class ModalUnlockIOSAudio extends React.PureComponent {
  static propTypes = {
    hideModal: PropTypes.func.isRequired,
    unlockIOSAudio: PropTypes.func.isRequired,
  };

  render() {
    const { hideModal, unlockIOSAudio } = this.props;

    return (
      <Modal
        onClose={() => {
          hideModal();
        }}
      >
        <div className={s.dialog_content}>
          <h2>Picoloop</h2>
          <p>Click to enter the Picoloop app.</p>
          <button
            onClick={() => {
              unlockIOSAudio();
              hideModal();
            }}
          >
            Ok
          </button>
        </div>
      </Modal>
    );
  }
}

export default ModalUnlockIOSAudio;
