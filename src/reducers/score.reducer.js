import {
  CREATE_PATTERN,
  CREATE_STEP,
  CREATE_TRACK,
  SELECT_PATTERN,
  SELECT_TRACK,
  SET_STEP_VALUE,
  SET_TRACK_VALUES,
  TOGGLE_STEP,
} from '../constants';
import { TRACK_GAIN, TRACK_TRANSPOSE } from '../constants/controlIds';
import {
  DEFAULT_GAIN,
  DEFAULT_PITCH,
  DEFAULT_TRANSPOSE,
} from '../constants/settings';

const initialState = {
  patternId: null,
  patterns: {
    allIds: [],
    byId: {},
  },
  steps: {
    allIds: [],
    byId: {},
  },
  trackId: null,
  tracks: {
    allIds: [],
    byId: {},
  },
};

export default function score(state = initialState, action) {
  switch (action.type) {
    case CREATE_PATTERN:
      return {
        ...state,
        patterns: {
          allIds: [...state.patterns.allIds, action.id],
          byId: {
            ...state.patterns.byId,
            [action.id]: {
              trackIds: [...action.trackIds],
            },
          },
        },
      };
    case CREATE_STEP:
      return {
        ...state,
        steps: {
          allIds: [...state.steps.allIds, action.id],
          byId: {
            ...state.steps.byId,
            [action.id]: {
              gain: DEFAULT_GAIN,
              pitch: DEFAULT_PITCH,
              selected: false,
              transpose: DEFAULT_TRANSPOSE,
            },
          },
        },
      };
    case CREATE_TRACK:
      return {
        ...state,
        tracks: {
          allIds: [...state.tracks.allIds, action.id],
          byId: {
            ...state.tracks.byId,
            [action.id]: {
              stepIds: [...action.stepIds],
            },
          },
        },
      };
    case SELECT_PATTERN:
      return {
        ...state,
        patternId: state.patterns.allIds[action.index],
      };
    case SELECT_TRACK:
      return {
        ...state,
        trackId: state.tracks.allIds[action.index],
      };
    case TOGGLE_STEP:
      return {
        ...state,
        steps: {
          allIds: [...state.steps.allIds],
          byId: state.steps.allIds.reduce((accumulator, stepId, index) => {
            const step = state.steps.byId[stepId];
            if (index === parseInt(action.stepId, 10)) {
              step.selected =
                typeof action.state === 'boolean'
                  ? action.state
                  : !step.selected;
            }
            return { ...accumulator, [stepId]: step };
          }, {}),
        },
      };
    case SET_STEP_VALUE: {
      const actionStepId =
        state.tracks.byId[state.trackId].stepIds[
          parseInt(action.controlId, 10)
        ];

      return {
        ...state,
        steps: {
          allIds: [...state.steps.allIds],
          byId: state.steps.allIds.reduce((accumulator, stepId) => {
            const step = state.steps.byId[stepId];
            if (stepId === actionStepId) {
              switch (action.editorType) {
                case TRACK_GAIN:
                  step.gain = Math.max(
                    0,
                    Math.min(step.gain + action.normalValueChange, 1),
                  );
                  break;
                case TRACK_TRANSPOSE:
                  step.transpose = Math.max(
                    -1,
                    Math.min(step.transpose + action.normalValueChange, 1),
                  );
                  break;
                default:
                // no editor type provided, so nothing to change
              }
            }
            return { ...accumulator, [stepId]: step };
          }, {}),
        },
      };
    }
    case SET_TRACK_VALUES: {
      const currentStepIds = state.tracks.byId[state.trackId].stepIds;
      return {
        ...state,
        steps: {
          allIds: [...state.steps.allIds],
          byId: state.steps.allIds.reduce((accumulator, stepId) => {
            const step = state.steps.byId[stepId];
            if (currentStepIds.indexOf(stepId) > -1) {
              switch (action.controlId) {
                case TRACK_GAIN:
                  step.gain = Math.max(
                    0,
                    Math.min(step.gain + action.normalValueChange, 1),
                  );
                  break;
                case TRACK_TRANSPOSE:
                  step.transpose = Math.max(
                    -1,
                    Math.min(step.transpose + action.normalValueChange, 1),
                  );
                  break;
                default:
                // unknown control id, so nothing to change
              }
            }
            return { ...accumulator, [stepId]: step };
          }, {}),
        },
      };
    }
    default:
      return state;
  }
}
