import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import PicoLoop from '../../app/PicoLoop';

import s from './Home.css';

@withStyles(s)
class Home extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <PicoLoop />
        </div>
      </div>
    );
  }
}

export default Home;
