import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './Instrument.css';

import Control from '../Control';

@withStyles(normalizeCss, s)
class Instrument extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <div className={s.root}>
        <Control id="iets" isSlider key="iets" text="!" selected={false} />
      </div>
    );
  }
}

export default Instrument;
