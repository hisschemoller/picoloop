import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import themableWithStyles from 'themableWithStyles';

import ButtonBase from '../ButtonBase';

import styles from './Button.css';

@themableWithStyles(styles)
class Button extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * If `true`, the button will be disabled.
     */
    disabled: PropTypes.bool,
    /**
     * If `true`, the button will be the full width.
     */
    wide: PropTypes.bool,
    /**
     * If `true`, the button will have round corners.
     */
    rounded: PropTypes.bool,
    /**
     * If `true`, the button will be in loading status.
     */
    loading: PropTypes.bool,
    /**
     * If `true`, the button will have round corners.
     */
    size: PropTypes.oneOf(['small', 'normal', 'large']),
    /**
     * The different button themes.
     */
    theme: PropTypes.oneOf([
      'default',
      'primary',
      'secondary',
      'success',
      'danger',
      'info',
      'warning',
    ]),
    /**
     * The different button views.
     */
    view: PropTypes.oneOf(['button', 'link', 'outline']),
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      wide: PropTypes.string,
      rounded: PropTypes.string,
      pending: PropTypes.string,
      small: PropTypes.string,
      normal: PropTypes.string,
      large: PropTypes.string,
      button: PropTypes.string,
      link: PropTypes.string,
      outline: PropTypes.string,
      default: PropTypes.string,
      primary: PropTypes.string,
      secondary: PropTypes.string,
      success: PropTypes.string,
      danger: PropTypes.string,
      info: PropTypes.string,
      warning: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    theme: 'default',
    view: 'button',
    size: 'normal',
    wide: false,
    rounded: false,
    loading: false,
    disabled: false,
    css: {
      root: styles.root,
      wide: styles.wide,
      rounded: styles.rounded,
      pending: styles.pending,
      small: styles.small,
      normal: styles.normal,
      large: styles.large,
      button: styles.button,
      link: styles.link,
      outline: styles.outline,
      default: styles.default,
      primary: styles.primary,
      secondary: styles.secondary,
      success: styles.success,
      danger: styles.danger,
      info: styles.info,
      warning: styles.warning,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      disabled,
      theme,
      size,
      wide,
      rounded,
      view,
      loading,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      css[theme],
      css[view],
      css[size],
      {
        [css.wide]: wide,
        [css.rounded]: rounded,
        [css.pending]: loading,
      },
      classNameProp,
    );

    return (
      <ButtonBase
        className={className}
        disabled={disabled || loading}
        {...rest}
      >
        <span>{children}</span>
      </ButtonBase>
    );
  }
}

export default Button;
