import { SELECT_EDITOR_TYPE } from '../constants';
import { TRACK_TRANSPOSE } from '../constants/controlIds';

const initialState = {
  editorType: TRACK_TRANSPOSE,
};

export default function stepeditor(state = initialState, action) {
  switch (action.type) {
    case SELECT_EDITOR_TYPE:
      return {
        ...state,
        editorType: action.editorType,
      };
    default:
      return state;
  }
}
