import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './Transport.css';

import { DEFAULT_BPM, LOOKAHEAD, PPQN } from '../../../constants/settings';
import { BPM, PLAY } from '../../../constants/controlIds';
import Control from '../Control';

import * as transportActions from '../../../actions/transport.actions';

const mapStateToProps = ({ transport }) => ({
  bpm: transport.bpm,
  isRunning: transport.isRunning,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...transportActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(mapStateToProps, mapDispatchToProps)
class Transport extends Component {
  static propTypes = {
    bpm: PropTypes.number,
    isRunning: PropTypes.bool,
    transportStep: PropTypes.func.isRequired,
  };

  static defaultProps = {
    bpm: DEFAULT_BPM,
    isRunning: false,
  };

  constructor(props) {
    super(props);

    this.beatInMilliseconds = 0;
    this.needsScan = false;
    this.origin = 0;
    this.position = 0;
    this.scanEnd = 0;
    this.scanStart = 0;
    this.stepInMilliseconds = 0;
    this.tickInMilliseconds = 0;
  }

  componentDidMount() {
    this.setBPM(DEFAULT_BPM);
    this.run();
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.isRunning !== nextProps.isRunning) {
      if (nextProps.isRunning) {
        this.start();
      } else {
        this.rewind();
      }
      return true;
    }
    if (this.props.bpm !== nextProps.bpm) {
      this.setBPM(Math.round(nextProps.bpm));
      return true;
    }
    return false;
  }

  /**
   * Set Beats Per Minute.
   * @param {Number} newBpm New value for BPM.
   */
  setBPM(newBpm) {
    this.beatInMilliseconds = 60000.0 / newBpm;
    this.stepInMilliseconds = this.beatInMilliseconds / 4;
    this.tickInMilliseconds = this.beatInMilliseconds / PPQN;
  }

  /**
   * Updated the playhead position by adjusting the timeline origin.
   * @param {Number} newOrigin Timeline origin timestamp.
   */
  setOrigin(newOrigin) {
    this.origin = newOrigin;
  }

  /**
   * Set the scan range.
   * @param {Number} start Start timestamp of scan range.
   */
  setScanRange(start) {
    this.needsScan = true;
    this.scanStart = start;
    this.scanEnd = start + LOOKAHEAD;
  }

  /**
   * Convert milliseconds to ticks.
   */
  msec2tick(sec) {
    return sec / this.tickInMilliseconds;
  }

  /**
   * Rewind the timer to timeline start.
   */
  rewind() {
    this.position = performance.now();
    this.setOrigin(this.position);
    this.setScanRange(this.position);
  }

  /**
   * Start the timer.
   */
  start() {
    this.rewind();
  }

  /**
   * Timer using requestAnimationFrame that updates the transport timing.
   */
  run() {
    if (this.props.isRunning) {
      this.position = performance.now();
      if (this.scanEnd - this.position < 16.7) {
        this.setScanRange(this.scanEnd);
      }
      if (this.needsScan) {
        this.needsScan = false;
        const { transportStep } = this.props;
        const startTimeInSteps =
          (this.scanStart - this.origin) / this.stepInMilliseconds;
        const endTimeInSteps =
          (this.scanEnd - this.origin) / this.stepInMilliseconds;
        let stepIndex = startTimeInSteps;
        while (Math.ceil(stepIndex) < endTimeInSteps) {
          stepIndex = Math.ceil(stepIndex);
          const offset = this.position - this.origin;
          const stepStart = stepIndex * this.stepInMilliseconds;
          const nowToStepStart = stepStart - offset;
          transportStep(stepIndex, nowToStepStart);
          stepIndex += 1;
        }
      }
    }
    requestAnimationFrame(this.run.bind(this));
  }

  render() {
    const { bpm, isRunning } = this.props;
    const label = isRunning ? '||' : '>';

    return (
      <div className={s.root}>
        <Control id={PLAY} isSlider={false} text={label} />
        <Control id={BPM} isSlider text={Math.round(bpm).toString()} />
      </div>
    );
  }
}

export default Transport;
