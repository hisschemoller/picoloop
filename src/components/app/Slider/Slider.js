import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import normalizeCss from 'normalize.css';
import s from './Slider.css';

import * as controlActions from '../../../actions/control.actions';

const mapStateToProps = ({ control }) => ({
  controlId: control.controlId,
  controlIsSlider: control.controlIsSlider,
  controlOffsetX: control.controlOffsetX,
  controlOffsetY: control.controlOffsetY,
  controlStartX: control.controlStartX,
  controlY: control.controlY,
  isDown: control.isDown,
  isTap: control.isTap,
  relativeValue: control.relativeValue,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...controlActions }, dispatch);

@withStyles(normalizeCss, s)
@connect(mapStateToProps, mapDispatchToProps)
class Slider extends PureComponent {
  render() {
    const {
      controlId,
      controlIsSlider,
      controlOffsetX,
      controlOffsetY,
      controlStartX,
      controlY,
      isDown,
      isTap,
      relativeValue,
    } = {
      ...this.props,
    };

    const hide =
      !isDown ||
      !controlIsSlider ||
      isTap ||
      controlId === null ||
      controlY === 0;

    const markerStyle = {
      left: `${controlStartX - controlOffsetX}px`,
      top: `${controlY - controlOffsetY}px`,
    };

    return hide ? null : (
      <div className={s.root}>
        <div className={s.layer} />
        <div
          className={classNames(s.marker, relativeValue ? s.show : '')}
          style={markerStyle}
        >
          <span className={s.label}>{relativeValue.toFixed(2)}</span>
        </div>
      </div>
    );
  }
}

export default Slider;
