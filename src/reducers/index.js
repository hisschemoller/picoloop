import { combineReducers } from 'redux';
import control from './control.reducer';
import nav from './nav.reducer';
import picoloop from './picoloop.reducer';
import score from './score.reducer';
import stepeditor from './stepeditor.reducer';
import transport from './transport.reducer';
import runtime from './runtime.reducer';

export default combineReducers({
  control,
  nav,
  picoloop,
  score,
  stepeditor,
  transport,
  runtime,
});
