/* eslint-disable import/prefer-default-export */

import { v1 as uuidv1 } from 'uuid';
import {
  CREATE_PATTERN,
  CREATE_STEP,
  CREATE_TRACK,
  SELECT_PATTERN,
  SELECT_TRACK,
  TOGGLE_STEP,
} from '../constants';
import { DEFAULT_TRACK_LENGTH, TRACKS } from '../constants/settings';
import makeActionCreator from './action-utils';

export const selectPattern = makeActionCreator(SELECT_PATTERN, 'index');
export const selectTrack = makeActionCreator(SELECT_TRACK, 'index');
export const toggleStep = makeActionCreator(TOGGLE_STEP, 'stepId', 'state');

function createStep(dispatch) {
  const stepId = uuidv1();
  dispatch({
    type: CREATE_STEP,
    id: stepId,
  });
  return stepId;
}

function createTrack(dispatch) {
  const id = uuidv1();
  const stepIds = [];
  for (let i = 0; i < DEFAULT_TRACK_LENGTH; i += 1) {
    const stepId = createStep(dispatch);
    stepIds.push(stepId);
  }
  dispatch({
    type: CREATE_TRACK,
    id,
    stepIds,
  });
  return id;
}

export function createPattern() {
  return dispatch => {
    const id = uuidv1();
    const trackIds = [];
    for (let j = 0; j < TRACKS; j += 1) {
      const trackId = createTrack(dispatch);
      trackIds.push(trackId);
    }
    dispatch({
      type: CREATE_PATTERN,
      id,
      trackIds,
    });
  };
}
