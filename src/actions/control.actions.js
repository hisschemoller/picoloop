/* eslint-disable import/prefer-default-export */

import {
  CONTROL_END,
  CONTROL_MOVE,
  CONTROL_START,
  SET_STEP_VALUE,
  SET_TRACK_VALUES,
  TRANSPORT_BPM,
} from '../constants';
import { SLIDER_TRACK_LENGTH } from '../constants/settings';
import { navSelect } from './nav.actions';
import { toggleStep } from './score.actions';
import { selectEditorType } from './stepeditor.actions';
import { transportToggle } from './transport.actions';

import {
  BPM,
  PLAY,
  NAV_SEQUENCER,
  NAV_STUDIO,
  TRACK_GAIN,
  TRACK_TRANSPOSE,
} from '../constants/controlIds';

export function controlStart(id, isSelected, isSlider, x, y, offsetX, offsetY) {
  return dispatch => {
    dispatch({
      type: CONTROL_START,
      id,
      isSelected: isSelected === 'true',
      isSlider,
      x,
      y,
      offsetX,
      offsetY,
    });

    if (!(isSelected === 'true')) {
      switch (id) {
        case PLAY:
          dispatch(transportToggle());
          break;
        case NAV_SEQUENCER:
        case NAV_STUDIO:
          dispatch(navSelect(id));
          break;
        case TRACK_GAIN:
        case TRACK_TRANSPOSE:
          dispatch(selectEditorType(id));
          break;
        default:
          if (!Number.isNaN(parseInt(id, 10))) {
            // it's a sequencer step
            dispatch(toggleStep(id, true));
          }
      }
    }
  };
}

export function controlEnd(isOnControl) {
  return (dispatch, getState) => {
    const { controlId, controlIsSelected } = getState().control;
    if (controlIsSelected && isOnControl) {
      switch (controlId) {
        default:
          if (!Number.isNaN(parseInt(controlId, 10))) {
            dispatch(toggleStep(controlId, false));
          }
      }
    }

    dispatch({ type: CONTROL_END });
  };
}

export function controlMove(x, y) {
  return (dispatch, getState) => {
    const state = getState();
    const { controlId, controlY } = state.control;
    const normalValueChange = (controlY - y) / SLIDER_TRACK_LENGTH;
    switch (controlId) {
      case BPM:
        dispatch({ type: TRANSPORT_BPM, normalValueChange });
        break;
      case TRACK_GAIN:
      case TRACK_TRANSPOSE:
        dispatch({ type: SET_TRACK_VALUES, controlId, normalValueChange });
        break;
      default:
        if (!Number.isNaN(parseInt(controlId, 10))) {
          // it's a sequencer step
          const { editorType } = state.stepeditor;
          dispatch({
            type: SET_STEP_VALUE,
            controlId,
            normalValueChange,
            editorType,
          });
        }
    }

    dispatch({ type: CONTROL_MOVE, x, y });
  };
}
