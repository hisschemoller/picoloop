/* eslint-disable import/prefer-default-export */

import { TRANSPORT_STEP, TRANSPORT_TOGGLE } from '../constants';
import makeActionCreator from './action-utils';

export const transportToggle = makeActionCreator(TRANSPORT_TOGGLE);
export const transportStep = makeActionCreator(
  TRANSPORT_STEP,
  'stepIndex',
  'nowToStep',
);
