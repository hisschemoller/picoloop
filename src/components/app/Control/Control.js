/* eslint-disable css-modules/no-unused-class */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styles from './Control.css';
import Button from '../../../rsk-components/elements/Button';

class Step extends PureComponent {
  static propTypes = {
    active: PropTypes.bool,
    children: PropTypes.node,
    id: PropTypes.string,
    isSlider: PropTypes.bool,
    isToggle: PropTypes.bool,
    selected: PropTypes.bool,
    text: PropTypes.string,
    theme: PropTypes.oneOf(['default', 'primary', 'secondary']),
  };

  static defaultProps = {
    active: false,
    children: null,
    id: null,
    isSlider: false,
    isToggle: false,
    selected: false,
    text: null,
    theme: 'default',
  };

  render() {
    const {
      active,
      children,
      id,
      isSlider,
      isToggle,
      selected,
      text,
      ...rest
    } = this.props;
    const selectedStyle = selected ? '#cce' : '';
    const style = {
      backgroundColor: active ? '#aea' : selectedStyle,
    };

    return (
      <Button
        {...rest}
        css={styles}
        style={style}
        data-control-id={id}
        data-is-selected={selected}
        data-is-slider={isSlider}
        data-is-toggle={isToggle}
      >
        <span>{text || children}</span>
      </Button>
    );
  }
}

export default Step;
