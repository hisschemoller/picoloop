/* eslint-disable import/prefer-default-export */

// control
export const CONTROL_END = 'CONTROL_END';
export const CONTROL_MOVE = 'CONTROL_MOVE';
export const CONTROL_START = 'CONTROL_START';

// nav
export const NAV_SELECT = 'NAV_SELECT';

// picoloop
export const INITIALIZE = 'INITIALIZE';
export const MODAL_SHOW = 'MODAL_SHOW';
export const MODAL_HIDE = 'MODAL_HIDE';

// score
export const CREATE_PATTERN = 'CREATE_PATTERN';
export const CREATE_STEP = 'CREATE_STEP';
export const CREATE_TRACK = 'CREATE_TRACK';
export const SELECT_PATTERN = 'SELECT_PATTERN';
export const SELECT_TRACK = 'SELECT_TRACK';
export const SET_STEP_VALUE = 'SET_STEP_VALUE';
export const SET_TRACK_VALUES = 'SET_TRACK_VALUES';
export const TOGGLE_STEP = 'TOGGLE_STEP';

// stepeditor
export const SELECT_EDITOR_TYPE = 'SELECT_EDITOR_TYPE';

// transport
export const TRANSPORT_STEP = 'TRANSPORT_STEP';
export const TRANSPORT_BPM = 'TRANSPORT_BPM';
export const TRANSPORT_TOGGLE = 'TRANSPORT_TOGGLE';

export const SET_RUNTIME_VARIABLE = 'SET_RUNTIME_VARIABLE';
